//
// Created by Dan on 06.12.2021.
//

#include "AreInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    are_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
