cmake_minimum_required(VERSION 3.10.2)
project(library-archiveext C)

set(CMAKE_C_STANDARD 99)

file(GLOB C archive-ext/*.c)
file(GLOB L */lib/${ANDROID_ABI})
set(l libarchive.a)
set(I libarchive/include)

add_library(library-archiveext SHARED ${C} ArchiveExt.h AreMain.c AreMain.h AreInit.c AreInit.h)
target_link_directories(library-archiveext PUBLIC ${L})
target_link_libraries(library-archiveext ${l} library-glibext)
target_include_directories(library-archiveext PUBLIC ${I} .)
